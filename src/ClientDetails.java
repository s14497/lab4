public class ClientDetails extends Entity{
	
	public ClientDetails(){
	}

	public long getID() {
		return ID;
	}
	public void setId(long id) {
		this.ID = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSurname() {
		return surname;
	}
	public void setSurname(String surname) {
		this.surname = surname;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	public ClientDetails(long id, String name, String surname, String login) {
		super();
		this.ID = id;
		this.name = name;
		this.surname = surname;
		this.login = login;
	}
	private long ID;
	private String name;
	private String surname;
	private String login;
}
