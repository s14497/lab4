import java.sql.Connection;
import java.sql.SQLException;

public class OrderRepository extends Repository<Order>{
	
	protected OrderRepository(Connection connection, IEntityBuilder<Order> builder, IUnitOfWork unitOfWork){
		super(connection, builder, unitOfWork);
	}
	
	
	protected void setUpUpdateQuery(Order entity) throws SQLException {
		update.setLong(1, entity.getClient().getID());
		update.setLong(2, entity.getDeliveryAddress().getID());
		update.setLong(4, entity.getID());
	}
	
	
	protected void setUpInsertQuery(Order entity) throws SQLException {
		insert.setLong(1, entity.getClient().getID());
		insert.setLong(2, entity.getDeliveryAddress().getID());
	}


	protected String getTableName() {
		return "Order";
	}


	protected String getUpdateQuery() {
		return "UPDATE Order SET (client, deliveryAddress, items) = (?, ?, ?) WHERE id = ?";
	}

	
	protected String getInsertQuery() {
		return "INSERT INTO Order VALUES (?, ?, ?)";
	}
}