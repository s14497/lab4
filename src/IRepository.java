import java.util.List;

public interface IRepository <TEntity>{
	
	public TEntity get(int id);
	public List <TEntity> getAll();
	public void save(TEntity e);
	public void update(TEntity e);
	public void delete(TEntity e);
}