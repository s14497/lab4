import java.sql.Connection;
import java.sql.SQLException;

public class AddressRepository extends Repository<Address>{
	
	protected AddressRepository(Connection connection, IEntityBuilder<Address> builder, IUnitOfWork unitOfWork){
		super(connection, builder, unitOfWork);
	}
	
	
	protected void setUpUpdateQuery(Address entity) throws SQLException {
		update.setString(1, entity.getStreet());
		update.setString(2, entity.getBuildingNumber());
		update.setString(3, entity.getFlatNumber());
		update.setString(4, entity.getPostalCode());
		update.setString(5, entity.getCity());
		update.setString(6, entity.getCountry());
		update.setLong(7, entity.getID());
	}


	protected void setUpInsertQuery(Address entity) throws SQLException {
		insert.setString(1, entity.getStreet());
		insert.setString(2, entity.getBuildingNumber());
		insert.setString(3, entity.getFlatNumber());
		insert.setString(4, entity.getPostalCode());
		insert.setString(5, entity.getCity());
		insert.setString(6, entity.getCountry());
	}


	protected String getTableName() {
		return "Address";
	}


	protected String getUpdateQuery() {
		return "UPDATE Address SET (street, buildingNumber, flatNumber, postalCode, city, country) = (?, ?, ?, ?, ?, ?) WHERE id = ?";
	}


	protected String getInsertQuery() {
		return "INSERT INTO Address (street, buildingNumber, flatNumber, postalCode, city, country) "
				+ "VALUES (?, ?, ?, ?, ?, ?)";
	}
}