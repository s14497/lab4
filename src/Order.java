import java.util.List;


public class Order extends Entity{
	public Order(){
	}
	
	public Order(long id, ClientDetails client, Address deliveryAddress, List<OrderItem> items) {
		super();
		this.ID = id;
		this.client = client;
		this.deliveryAddress = deliveryAddress;
		this.items = items;
	}
	private long ID;
	private ClientDetails client;
	private Address deliveryAddress;
	private List<OrderItem> items;
	public long getID() {
		return ID;
	}
	public void setId(long id) {
		this.ID = id;
	}
	public ClientDetails getClient() {
		return client;
	}
	public void setClient(ClientDetails client) {
		this.client = client;
	}
	public Address getDeliveryAddress() {
		return deliveryAddress;
	}
	public void setDeliveryAddress(Address deliveryAddress) {
		this.deliveryAddress = deliveryAddress;
	}
	public List<OrderItem> getItems() {
		return items;
	}
	public void setItems(List<OrderItem> items) {
		this.items = items;
	}
}