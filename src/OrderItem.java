public class OrderItem extends Entity{
	public OrderItem(long id, String name, String description, Double price) {
		super();
		this.ID = id;
		this.name = name;
		this.description = description;
		this.price = price;
	}
	
	public OrderItem(){
		
	}
	
	private long ID;
	private String name;
	private String description;
	private Double price;
	public long getID() {
		return ID;
	}
	public void setId(long id) {
		this.ID = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Double getPrice() {
		return price;
	}
	public void setPrice(Double price) {
		this.price = price;
	}
}