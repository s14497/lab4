import java.sql.Connection;
import java.sql.SQLException;

public class OrderItemRepository extends Repository<OrderItem>{
	
	protected OrderItemRepository(Connection connection, IEntityBuilder<OrderItem> builder, IUnitOfWork UnitOfWork){
		super(connection, builder, UnitOfWork);
	}
	
	
	protected void setUpUpdateQuery(OrderItem entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setString(2, entity.getDescription());
		update.setDouble(3, entity.getPrice());
		update.setLong(4, entity.getID());
	}


	protected void setUpInsertQuery(OrderItem entity) throws SQLException {
		insert.setString(1, entity.getName());
		insert.setString(2, entity.getDescription());
		insert.setDouble(3, entity.getPrice());
	}


	protected String getTableName() {
		return "OrderItem";
	}


	protected String getUpdateQuery() {
		return "UPDATE OrderItem SET (name, description, price) = (?, ?, ?) WHERE id = ?";
	}


	protected String getInsertQuery() {
		return "INSERT INTO OrderItem VALUES (?, ?, ?)";
	}
}