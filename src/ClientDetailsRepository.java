import java.sql.Connection;
import java.sql.SQLException;

public class ClientDetailsRepository extends Repository<ClientDetails>{
	
	protected ClientDetailsRepository(Connection connection, IEntityBuilder<ClientDetails> builder, IUnitOfWork UnitOfWork){
		super(connection, builder, UnitOfWork);
	}
	

	protected void setUpUpdateQuery(ClientDetails entity) throws SQLException {
		update.setString(1, entity.getName());
		update.setString(2, entity.getSurname());
		update.setString(3, entity.getLogin());
		update.setLong(4, entity.getID());
	}


	protected void setUpInsertQuery(ClientDetails entity) throws SQLException {
		insert.setString(1, entity.getName());
		insert.setString(2, entity.getSurname());
		insert.setString(3, entity.getLogin());
	}


	protected String getTableName() {
		return "ClientDetails";
	}


	protected String getUpdateQuery() {
		return "UPDATE ClientDetails SET (name, surname, login) = (?, ?, ?) WHERE id = ?";
	}


	protected String getInsertQuery() {
		return "INSERT INTO ClientDetails VALUES (?, ?, ?)";
	}
}